# Documentação e Artefatos

## Este documento visa apresentar o projeto NetWorkMe.

### O sistema encontra-se [neste link](http://networkme-app.herokuapp.com)

### Artigo do projeto [encontrado no medium](https://medium.com/@grxgabriel/projeto-final-de-ads-f92cbd4c1e71?postPublishedType=repub)

# Artefatos:

## [Apresentação](apresentacao.md)

## [Planejamento](planejamento.md)

## [Arquitetura do Sistema](arquitetura.md)

## [Modelagem de Dados](modelagem-dados.md)

## [Tabela Comparativa de Sistemas Correlatos](tabela-comparativa.md)
