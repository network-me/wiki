<h1> Modelagem de Dados </h1>

## Usuários

```JSON
{
  "name": {
    "type": "String",
    "required": true
  },
  "email": {
    "type": "String",
    "required": true,
    "unique": true
  },
  "password": {
    "type": "String",
    "required": true
  },
  "favoriteProjects": [
    {
      "type": "ObjectId",
      "ref": "Project"
    }
  ],
}
```

## Projetos

```JSON
{
  "title": {
    "type": "String",
    "required": true
  },
  "slug": {
    "type": "String",
    "unique": true
  },
  "categories": [
    {
      "type": "String",
      "required": true
    }
  ],
  "phone": {
    "type": "String"
  },
  "email": {
    "type": "String"
  },
  "necessaryValue": {
    "type": "String"
  },
  "pitch": {
    "type": "String",
    "required": true
  },
  "about": {
    "type": "String",
    "required": true
  },
  "banner": {
    "type": "String"
  },
  "kickstarterUrl": {
    "type": "String"
  },
  "catarseUrl": {
    "type": "String"
  },
  "author": {
    "type": "ObjectId",
    "ref": "User",
    "required": true
  },
  "createdAt": {
    "type": Date,
    "default": new Date()
  }
}
```

## Metadados

```JSON
{
  "user": {
    "type": "ObjectId",
    "ref": "User",
    "required": true
  },
  "type": {
    "type": "String",
    "required": true
  },
  "data": {
    "type": "String",
    "required": true
  }
}
```

## Dialogos

```JSON
{
  "userMessage": {
    "type": "String",
    "required": true
  },
  "botMessage": {
    "type": "String",
    "required": true
  },
  "user": {
    "type": "ObjectId",
    "ref": "User",
    "required": true
  },
  "createdAt": {
    "type": Date,
    "default": Date.now
  }
}
```

## Newsletter

```JSON
{
  "email": {
    "type": "String",
    "required": true,
    "unique": true
  },
  "createdAt": {
    "type": Date,
    "default": Date.now
  }
}
```
