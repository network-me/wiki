<h1> Tabela Comparativa </h1>

## <table>

  <thead>
    <tr style='text-align: center'>
      <td> x </td>
      <td> <strong>NetWorkMe</strong> </td>
      <td> <strong>Catarse</strong> </td>
      <td> <strong>Kickstarter</strong> </td>
      <td> <strong>Ulule</strong> </td>
    </tr>
  </thead>
  <tbody>
    <tr style='text-align: center'>
      <td>Sistema Web</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr style='text-align: center'>
      <td>Investimento Coletivo</td>
      <td> </td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
    </tr>
    <tr style='text-align: center'>
      <td>Português</td>
      <td>X</td>
      <td> </td>
      <td> </td>
      <td>X</td>
    </tr>
    <tr style='text-align: center'>
      <td>Ferramenta de Auxílio</td>
      <td>X</td>
      <td> </td>
      <td> </td>
      <td> </td>
    </tr>   
  </tbody>
</table>
